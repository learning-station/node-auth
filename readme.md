# node-auth

## curl

```sh
curl -v -X POST localhost:3000/register -H 'Content-Type: application/json' -d \
'{"name": "Bryan", "email": "bryandms@example.com", "password": "Your.secure.password.1", "passwordConfirmation": "Your.secure.password.1"}'
```

```sh
curl -v -X POST localhost:3000/login -H 'Content-Type: application/json' -d \
'{"email":"bryandms@example.com", "password":"Your.secure.password.1"}'
```
